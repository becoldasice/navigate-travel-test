import { createGlobalStyle } from 'styled-components'

/* eslint no-unused-expressions: 0 */
export default createGlobalStyle`
  html,
  body {
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    -webkit-font-smoothing: antialiased;
  }

  body.fontLoaded {
    font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    display: flex;
    flex-flow: column;
    background-color: white;
    position: relative;
    width: 100%;
  }

  /* react-day-picker styles*/
  .DayPickerInput {
    display: block;
    width: 100%;
    align-self: flex-start
    text-align: center;
  }
  .DayPickerInput > input {
    width: 50%;
  }

  .DayPickerInput-Overlay {
    position: absolute;
    width: 50%;
    margin-left: -25%;
    left: 50%;
  }

  .DayPicker,
  .DayPicker-Months,
  .DayPicker-Month {
    width: 100%;
  }

  .DayPickerInput > input {
    padding: 12px 6px;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: 5px;
    text-align: center;
  }
`
