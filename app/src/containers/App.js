import React from 'react'

import DayPickerInput from 'react-day-picker/DayPickerInput'

import 'react-day-picker/lib/style.css'

import loader from '@assets/img/loader.svg'

import GlobalStyles from 'global-styles'
import Loader from 'components/Loader'
import Wrapper from 'components/Wrapper'
import Hero from 'components/Hero'
import BaseContainer from 'components/BaseContainer'
import ResultsContainer from 'components/Results'

import perform from 'services/api'

class App extends React.Component {
  state = {
    results: [],
    loading: false,
    ready: false,
  }

  disabledDays = { before: new Date() }

  handleDayChange = (date) => {
    const d = new Date(date)
    let month = `${d.getMonth() + 1}`
    let day = `${d.getDate()}`
    const year = d.getFullYear()

    if (month.length < 2) month = `0${month}`
    if (day.length < 2) day = `0${day}`

    const searchDate = [year, month, day].join('-')

    this.setState(
      {
        loading: true,
        results: [],
      },
      () => {
        perform(`searchDate=${searchDate}`).then(({ data }) => {
          this.setState({
            results: data,
            loading: false,
            ready: data.length > 0,
          })
        })
      },
    )
  }

  render() {
    const { results, loading, ready } = this.state
    return (
      <Wrapper>
        <BaseContainer>
          {!loading && results.length === 0 && (
            <Hero>
              Find the <b>perfect</b> Yacht to experience your trip
            </Hero>
          )}
          <DayPickerInput
            parseDate={this.parseDate}
            onDayChange={this.handleDayChange}
            dayPickerProps={{ disabledDays: this.disabledDays }}
            inputProps={{
              placeholder: 'YYYY-MM-DD',
            }}
          />
          {loading && results.length === 0 && <Loader />}
          {!loading && results.length > 0 && (
            <>
              <h4>{`${results.length} dates avaliable`}</h4>
              <ResultsContainer results={results} />
            </>
          )}
        </BaseContainer>
        <GlobalStyles ready={ready} />
      </Wrapper>
    )
  }
}

export default App
