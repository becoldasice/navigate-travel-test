import axios from 'axios'

const BASE_URL = 'http://api.ntstage.com/v1/bookingsweb/availability?routeId=4&amountResultsAfter=100&amountResultsBefore=0'

const NTAxios = axios.create({
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})

export default (urlQueryParams) =>
  NTAxios({
    url: `${BASE_URL}&${urlQueryParams}`,
  }).then((res) => res.data)
