import React from 'react'
import { render } from 'react-dom'
import FontFaceObserver from 'fontfaceobserver'
import 'sanitize.css/sanitize.css'
import App from 'containers/App'

const MontSerratObserver = new FontFaceObserver('Montserrat', {})

MontSerratObserver.load().then(
  () => {
    document.body.classList.add('fontLoaded')
  },
  () => {
    document.body.classList.remove('fontLoaded')
  },
)

render(<App />, document.getElementById('app'))
