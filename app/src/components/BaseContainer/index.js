import React, { memo } from 'react'
import styled from 'styled-components'

const Wrapper = styled.section`
  width: 50%;
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: center;
  position: relative;
  z-index: 3;
  transition: transform 0.8s ease-in-out;
  padding: 16px;
  background-color: rgb(220, 243, 255);
  height: 100vh;
  overflow: scroll;

  > div:not(:first-of-type) {
    margin: 6px 0;
  }
`

export default memo(({ children }) => <Wrapper>{children}</Wrapper>)
