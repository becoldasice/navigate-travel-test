import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ResultsContainer = styled.div`
  width: 100%;
  cursor: pointer;
  background-color: #81b2c9;
  border: 1px solid #dcf3ff;
  border-radius: 5px;
  max-height: ${(props) => (props.opened ? '280px' : '40px')};
  padding: 12px;
  color: white;
  overflow: hidden;
  display: flex;
  flex-flow: row wrap;
  transition: max-height 0.4s ease-in-out;
`

const ResultsContainerTitle = styled.span`
  color: white;
  font-weight: 700;
  width: 100%;
  display: flex;
`

const ResultContainerTitleAppend = styled.span`
  margin-left: auto;
`

const ResultContainerItem = styled.div`
  width: 100%;
  text-decoration: ${(props) => (props.soldOut ? 'line-through' : 'none')};
  color: ${(props) => (props.soldOut ? '#793200' : 'white')};

  &:not(:last-child) {
    margin: 12px 0;
  }
`

const Price = styled.span`
  font-weight: ${(props) => (props.emphasize ? 700 : 400)};
  color: currentColor
  width: 100%;
  display: block;
  &:not(:last-child) {
    margin: 6px 0;
  }
`

class Results extends React.PureComponent {
  state = {
    opened: null,
  }

  openContainer = (date) => () => {
    const { opened } = this.state
    this.setState({
      opened: date === opened ? null : date,
    })
  }

  render() {
    const { results } = this.props
    const { opened } = this.state

    return results.map((result) => (
      <ResultsContainer key={result.date} opened={opened === result.date} onClick={this.openContainer(result.date)}>
        <ResultsContainerTitle>
          {`${result.dateText} (${result.dateday})`}
          <ResultContainerTitleAppend>{result.temperature}</ResultContainerTitleAppend>
        </ResultsContainerTitle>
        {result.products.map((product) => {
          const status = Number(product.statusCode)
          const soldOut = status === 3
          const lastSpaces = status === 2
          const available = status === 1
          const price = product.prices[0]
          const differentPrice = price.rrp !== price.rrpWithDiscount

          return (
            <ResultContainerItem soldOut={soldOut} available={available} key={`${result.date}-${product.productClassId}`}>
              {`${product.productClass}: ${lastSpaces ? `${<b>(Last spaces!!)</b>}` : ''}`}
              <br />
              {(available || !soldOut) &&
                (differentPrice ? (
                  <>
                    <Price>Regular Price: {`${price.currencySymbol}${price.rrp}`}</Price>
                    <Price emphasize>Our Special Price: {`${price.currencySymbol}${price.rrpWithDiscount}`}</Price>
                  </>
                ) : (
                  <Price emphasize>Price: {`${price.currencySymbol}${price.rrpWithDiscount}`}</Price>
                ))}
              {soldOut && <Price>Unavailable ):</Price>}
            </ResultContainerItem>
          )
        })}
      </ResultsContainer>
    ))
  }
}

Results.propTypes = {
  results: PropTypes.array.isRequired,
}

export default Results
