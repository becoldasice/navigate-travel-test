import styled from 'styled-components'
import loaderSrc from '@assets/img/loader.svg'

export default styled.img.attrs({
  src: loaderSrc,
})`
  margin-bottom: auto;
`
