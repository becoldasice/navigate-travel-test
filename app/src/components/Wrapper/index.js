import styled from 'styled-components'

import bg from '@assets/img/bg.jpg'

export default styled.main`
  width: 100vw;
  height: 100vh;
  background: url(${bg}) center center no-repeat;
  background-size: cover;
  display: flex;
  flex-flow: row wrap;
  position: relative;
`
