import styled from 'styled-components'

export default styled.h1`
  margin: 12px 0;
  font-size: 58px;
  font-weight: 400;
  color: rgba(0, 0, 0, 0.95);
  text-align: center;
  align-self: flex-end;
`
