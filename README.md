## Instructions

- Install the project dependencies with `yarn install`
- Run `webpack-dev-server` with `yarn start`
- Access `localhost:8080`